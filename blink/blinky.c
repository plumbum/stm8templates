/********************************************
 * Original Example get from
 *   https://github.com/vdudouyt/sdcc-examples-stm8
 * modified by Ivan A-R
 *   https://bitbucket.org/plumbum/stm8templates
 ********************************************/

#include "stm8l.h"

int main(void)
{
    unsigned long int d;

	// Configure pins
	PC_DDR = 0x80;
	PC_CR1 = 0x80;
	PE_DDR = 0x80;
	PE_CR1 = 0x80;

	// Loop
	for(;;)
    {
		PE_ODR ^= 0x80;
		for(d = 0; d < 100000; d++) { }
		PC_ODR ^= 0x80;
		for(d = 0; d < 50000; d++) { }
	}
}

