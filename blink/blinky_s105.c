/********************************************
 * Original Example get from
 *   https://github.com/vdudouyt/sdcc-examples-stm8
 * modified by Ivan A-R
 *   https://bitbucket.org/plumbum/stm8templates
 ********************************************/

#include "stm8s105.h"

int main(void)
{
    unsigned long int d;

	// Configure pins
	PD_DDR = 0x01;
	PD_CR1 = 0x01;

	// Loop
	for(;;)
    {
		PD_ODR ^= 0x01;
		for(d = 0; d < 90000; d++) { }
		PD_ODR ^= 0x01;
		for(d = 0; d < 30000; d++) { }
	}
}

