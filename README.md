STM8 templates
==============

See also [http://tuxotronic.org/wiki/component/stm8]()

Get SDCC
--------

Download latest [SDCC snapshot](http://sdcc.sourceforge.net/snap.php) from [SDCC site](http://sdcc.sourceforge.net/)

Unpack to /usr/local

Add to PATH

  export PATH=/usr/local/sdcc/bin:$PATH

Check version

  $ sdcc -v
  SDCC : mcs51/z80/z180/r2k/r3ka/gbz80/tlcs90/ds390/pic16/pic14/TININative/ds400/hc08/s08/stm8 3.4.0/*rc1*/ #8960 (Mar 12 2014) (Linux)


Get and build stm8flash
-----------------------

Init and update submodule

  $ git submodule init
  $ git submodule update

Build

  $ cd stm8flash
  $ make

Install

  # make install


hex2bin
-------

Get latest hex2bin from [site](http://hex2bin.sourceforge.net/) or use local copy.

Rebuild binaries:

  $ cd Hex2bin-1.0.10/
  $ make clean
  $ make

Install it:

  # make install


Build sample
------------

  $ cd blink

Compile

  $ sdcc -mstm8 -lstm8 --out-fmt-ihx -o blinky.ihx blinky.c

Convert ihx to bin

  $ hex2bin -p 00 blinky.ihx

Flash firmware

  $ stm8flash -cstlink -pstm8l150 -w blinky.bin

Or use Makefile

  $ make flash

